package com.uni.cicduniversityfriendly.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.uni.cicduniversityfriendly.components.UniversityToolbar
import com.uni.cicduniversityfriendly.components.UniversityToolbarStyle
import com.uni.cicduniversityfriendly.components.UniversityToolbarType
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<T : ViewModel>: AppCompatActivity(), ToolbarListeners {

    private var navHostFragment: NavHostFragment? = null
    var navController: NavController? = null

    private var toolbar: UniversityToolbar? = null

    val viewModel: T by lazy {
        ViewModelProvider(this)[getTClass()]
    }

    private fun getTClass(): Class<T> {
        return (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>
    }

    fun initializeActivity(mode: UniversityToolbarType, title: String, style: UniversityToolbarStyle) {
        setupUi()

        when (mode) {
            UniversityToolbarType.NO_TOOLBAR -> {
                loadNoToolbarMode()
            }
            UniversityToolbarType.HOME -> {
                loadHomeToolbar(style)
            }
            UniversityToolbarType.NORMAL -> {
                loadDefaultToolbar(title, style)
            }
        }
    }

    fun initializeActivity() {
        setupUi()

        loadNoToolbarMode()
    }

    private fun setupUi() {

    }

    private fun loadNoToolbarMode() {
        toolbar?.let {
            it.visibility = View.GONE
        }
    }

    private fun loadHomeToolbar(style: UniversityToolbarStyle) {
        toolbar?.let {
            it.initToolbar(UniversityToolbarType.HOME, style)
            it.showRightButton {
                //rightButton(R.id.action_go_profile)
            }
        }
    }

    private fun loadDefaultToolbar(title: String, style: UniversityToolbarStyle) {
        toolbar?.let {
            it.initToolbar(UniversityToolbarType.NORMAL, title, style)
            it.showBackButton {
                onBackPressed()
            }
        }
    }

    override fun rightButton(action: Int) {

    }

    override fun leftButton(action: Int) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

}

interface ToolbarListeners {
    fun rightButton(action: Int)
    fun leftButton(action: Int)
}