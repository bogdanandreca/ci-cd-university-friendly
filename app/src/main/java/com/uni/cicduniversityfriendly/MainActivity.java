package com.uni.cicduniversityfriendly;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.uni.cicduniversityfriendly.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}