package com.uni.cicduniversityfriendly.splash_screen

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.widget.AppCompatImageView
import com.uni.cicduniversityfriendly.R
import com.uni.cicduniversityfriendly.authorization.LoginActivity
import com.uni.cicduniversityfriendly.base.BaseActivity


class SplashActivity: BaseActivity<SplashViewModel>(){

    private lateinit var appLogo: AppCompatImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        setupUi()

        initializeActivity()
        simulateLogo()
    }

    private fun setupUi(){
        appLogo = findViewById(R.id.splash_image)
        appLogo.setImageResource(R.drawable.app_logo)
    }

    private fun simulateLogo(){
        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                //set the new Content of your activity
                goLogin()
            }
        }.start()
    }

    private fun goLogin(){
        val intent = Intent(this, LoginActivity::class.java)
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }
}