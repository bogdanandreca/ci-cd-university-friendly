package com.uni.cicduniversityfriendly.notes

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.uni.cicduniversityfriendly.R
import com.uni.cicduniversityfriendly.base.BaseActivity
import com.uni.cicduniversityfriendly.home.HomeViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class AddNoteActivity : BaseActivity<HomeViewModel>(){
    var fStore: FirebaseFirestore? = null
    var noteTitle: EditText? = null
    lateinit var noteContent: EditText
    var progressBarSave: ProgressBar? = null
    var user: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)
        fStore = FirebaseFirestore.getInstance()
        noteContent = findViewById<EditText>(R.id.addNoteContent)
        noteTitle = findViewById<EditText>(R.id.addNoteTitle)
        progressBarSave = findViewById<ProgressBar>(R.id.progressBar)
        user = FirebaseAuth.getInstance().currentUser
        val fab: FloatingActionButton = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view: View? ->
            val nTitle = noteTitle!!.text.toString()
            val nContent: String = noteContent.getText().toString()
            if (nTitle.isEmpty() || nContent.isEmpty()) {
                Toast.makeText(
                    this,
                    "Can not Save note with Empty Field.",
                    Toast.LENGTH_SHORT
                )
                    .show()
                return@setOnClickListener
            }
            progressBarSave!!.visibility = View.VISIBLE

            // save note
            val docref =
                fStore!!.collection("notas").document(
                    user!!.uid
                ).collection("misnotas").document()
            val note: MutableMap<String, Any> =
                HashMap()
            note["title"] = nTitle
            note["content"] = nContent
            docref.set(note)
                .addOnSuccessListener { aVoid: Void? ->
                    progressBarSave!!.visibility = View.GONE
                    Toast.makeText(this, "Note Added.", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }.addOnFailureListener { e: Exception? ->
                    progressBarSave!!.visibility = View.GONE
                    Toast.makeText(this, "Error, Try again.", Toast.LENGTH_SHORT).show()
                }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = getMenuInflater()
        inflater.inflate(R.menu.close_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.close) {
            Toast.makeText(this, "Not Saved.", Toast.LENGTH_SHORT).show()
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
