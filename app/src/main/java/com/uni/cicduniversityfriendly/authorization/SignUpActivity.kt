package com.uni.cicduniversityfriendly.authorization


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.uni.cicduniversityfriendly.R
import com.uni.cicduniversityfriendly.base.BaseActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore


class SignUpActivity : BaseActivity<LoginViewModel>() {
    private lateinit var textInputEditTextFullName: TextInputEditText
    private lateinit var textInputEditTextEmail: TextInputEditText
    private lateinit var textInputEditTextUsername: TextInputEditText
    private lateinit var textInputEditTextPassword: TextInputEditText
    private lateinit var btnSignUp: Button
    private lateinit var textViewLogin: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var firebaseFirestore: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var userID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        setupUi()
    }

    private fun setupUi(){
        textInputEditTextFullName = findViewById(R.id.fullname)
        textInputEditTextUsername = findViewById(R.id.username)
        textInputEditTextPassword = findViewById(R.id.password)
        textInputEditTextEmail = findViewById(R.id.email)
        btnSignUp = findViewById(R.id.buttonSignUp)
        textViewLogin = findViewById(R.id.loginText)
        progressBar = findViewById(R.id.progress)
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseFirestore = FirebaseFirestore.getInstance()


        textViewLogin.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        }

        if (firebaseAuth.currentUser != null) {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        btnSignUp.setOnClickListener {
            val fullname: String = textInputEditTextFullName.text.toString()
            val username: String = textInputEditTextUsername.text.toString()
            val password: String = textInputEditTextPassword.text.toString()
            val email: String = textInputEditTextEmail.text.toString()

            if (fullname != "" && username != "" && password != "" && email != "") {
                progressBar.visibility = View.VISIBLE
                firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            progressBar.visibility = View.GONE
                            Toast.makeText(this@SignUpActivity, "Usuario creado", Toast.LENGTH_SHORT)
                                .show()
                            userID = firebaseAuth.currentUser?.uid ?: ""
                            val documentReference: DocumentReference =
                                firebaseFirestore.collection("usuarios").document(userID)
                            val usuario: MutableMap<String, Any> = HashMap()
                            usuario["fullName"] = fullname
                            usuario["userName"] = username
                            usuario["email"] = email
                            documentReference.set(usuario)
                                .addOnSuccessListener {
                                    val Tag = ""
                                    Log.d(
                                        Tag,
                                        "onSuccess: el usuario ha sido creado:$userID"
                                    )
                                }
                            val intent = Intent(applicationContext, LoginActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            progressBar.visibility = View.GONE
                            Toast.makeText(
                                this@SignUpActivity,
                                "Error " + task.exception?.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(
                    applicationContext,
                    "Todos los campos necesitan ser completados",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}