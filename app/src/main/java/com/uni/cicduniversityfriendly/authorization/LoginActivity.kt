package com.uni.cicduniversityfriendly.authorization


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.uni.cicduniversityfriendly.R
import com.uni.cicduniversityfriendly.base.BaseActivity
import com.uni.cicduniversityfriendly.home.HomeActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth


class LoginActivity : BaseActivity<LoginViewModel>() {
    private lateinit var textInputEditTextEmail: TextInputEditText
    private lateinit var textInputEditTextPassword: TextInputEditText
    private lateinit var buttonLogin: Button
    private lateinit var textViewSignUp: TextView
    private lateinit var progressBar: ProgressBar
    private var firebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupUi()
    }

    private fun setupUi(){
        textInputEditTextEmail = findViewById(R.id.email)
        textInputEditTextPassword = findViewById(R.id.password)
        buttonLogin = findViewById(R.id.buttonLogin)
        textViewSignUp = findViewById(R.id.signUpText)
        progressBar = findViewById(R.id.progress)

        textViewSignUp.setOnClickListener {
            val intent = Intent(applicationContext, SignUpActivity::class.java)
            startActivity(intent)
        }
        buttonLogin.setOnClickListener(View.OnClickListener { view: View? ->
            val email: String = textInputEditTextEmail.text.toString()
            val password: String = textInputEditTextPassword.text.toString()
            if (email != "" && password != "") {
                progressBar.visibility = View.VISIBLE
                firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            progressBar.visibility = View.GONE
                            Toast.makeText(this@LoginActivity, "Conectado!", Toast.LENGTH_SHORT).show()
                            val intent = Intent(applicationContext, HomeActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            progressBar.visibility = View.GONE
                            Toast.makeText(
                                this@LoginActivity,
                                "Error " + task.exception?.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(
                    applicationContext,
                    "Todos los campos necesitan ser completados",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }
}