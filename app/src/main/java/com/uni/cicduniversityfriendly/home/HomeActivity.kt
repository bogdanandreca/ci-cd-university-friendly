package com.uni.cicduniversityfriendly.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.uni.cicduniversityfriendly.R
import com.uni.cicduniversityfriendly.base.BaseActivity
import com.uni.cicduniversityfriendly.notes.AddNote
import com.uni.cicduniversityfriendly.notes.EditNote
import com.uni.cicduniversityfriendly.notes.NoteDetails
import com.uni.cicduniversityfriendly.notes.notes_model.Notes
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import java.util.*

class HomeActivity : BaseActivity<HomeViewModel>(){

    private lateinit var noteLists: RecyclerView
    private var fStore: FirebaseFirestore? = null
    private var noteAdapter: FirestoreRecyclerAdapter<Notes, NoteViewHolder>? = null
    private var user: FirebaseUser? = null
    private var fAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
        setupUi()

    }

    private fun setupUi(){
        fStore = FirebaseFirestore.getInstance()
        fAuth = FirebaseAuth.getInstance()
        user = fAuth!!.currentUser

        loadNotes()
        noteLists = findViewById(R.id.notelist)
        noteLists.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        noteLists.adapter = noteAdapter

        val fab = findViewById<FloatingActionButton>(R.id.addNoteFloat)
        fab.setOnClickListener { view ->
            startActivity(Intent(view.context, AddNote::class.java))
            overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
            finish()
        }

    }

    class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var noteTitle: TextView
        var noteContent: TextView
        var view: View
        var mCardView: CardView

        init {
            noteTitle = itemView.findViewById(R.id.titles)
            noteContent = itemView.findViewById(R.id.content)
            mCardView = itemView.findViewById(R.id.noteCard)
            view = itemView
        }
    }

    private fun loadNotes(){
        val query = fStore!!.collection("notas").document(
            user!!.uid
        ).collection("misnotas").orderBy("title", Query.Direction.DESCENDING)
        // query notes > uuid > mynotes

        // query notes > uuid > mynotes
        val allNotes = FirestoreRecyclerOptions.Builder<Notes>()
            .setQuery(query, Notes::class.java)
            .build()


        noteAdapter = object : FirestoreRecyclerAdapter<Notes, NoteViewHolder>(allNotes) {
            @RequiresApi(api = Build.VERSION_CODES.M)
            override fun onBindViewHolder(
                noteViewHolder: NoteViewHolder,
                @SuppressLint("RecyclerView") i: Int,
                note: Notes
            ) {
                noteViewHolder.noteTitle.text = note.title
                noteViewHolder.noteContent.text = note.content
                val code: Int = getRandomColor()
                noteViewHolder.mCardView.setCardBackgroundColor(
                    noteViewHolder.view.resources.getColor(
                        code,
                        null
                    )
                )
                val docId = noteAdapter!!.snapshots.getSnapshot(i).id
                noteViewHolder.view.setOnClickListener { v ->
                    val i = Intent(v.context, NoteDetails::class.java)
                    i.putExtra("title", note.title)
                    i.putExtra("content", note.content)
                    i.putExtra("code", code)
                    i.putExtra("noteId", docId)
                    v.context.startActivity(i)
                }
                val menuIcon = noteViewHolder.view.findViewById<ImageView>(R.id.menuIcon)
                menuIcon.setOnClickListener { v ->
                    val docId = noteAdapter!!.snapshots.getSnapshot(i).id
                    val menu = PopupMenu(v.context, v)
                    menu.gravity = Gravity.END
                    menu.menu.add("Edit").setOnMenuItemClickListener {
                        val i = Intent(v.context, EditNote::class.java)
                        i.putExtra("title", note.title)
                        i.putExtra("content", note.content)
                        i.putExtra("noteId", docId)
                        startActivity(i)
                        false
                    }
                    menu.menu.add("Delete").setOnMenuItemClickListener {
                        val docRef = fStore!!.collection("notas").document(
                            user!!.uid
                        ).collection("misnotas").document(docId)
                        docRef.delete().addOnSuccessListener {
                            // note deleted
                        }.addOnFailureListener {
                            Toast.makeText(
                                this@HomeActivity,
                                "Error in Deleting Note.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        false
                    }
                    menu.show()
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.note_view_layout, parent, false)
                return NoteViewHolder(view)
            }
        }
    }

    private fun getRandomColor(): Int {
        val colorCode: MutableList<Int> = ArrayList()
        colorCode.add(R.color.blue)
        colorCode.add(R.color.yellow)
        colorCode.add(R.color.skyblue)
        colorCode.add(R.color.lightPurple)
        colorCode.add(R.color.lightGreen)
        colorCode.add(R.color.gray)
        colorCode.add(R.color.pink)
        colorCode.add(R.color.red)
        colorCode.add(R.color.greenlight)
        colorCode.add(R.color.notgreen)
        val randomColor = Random()
        val number = randomColor.nextInt(colorCode.size)
        return colorCode[number]
    }

    override fun onStart() {
        super.onStart()
        noteAdapter!!.startListening()
    }

    override fun onStop() {
        super.onStop()
        if (noteAdapter != null) {
            noteAdapter!!.stopListening()
        }
    }
}