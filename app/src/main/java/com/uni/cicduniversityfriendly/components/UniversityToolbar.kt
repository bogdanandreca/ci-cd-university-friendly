package com.uni.cicduniversityfriendly.components

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import com.uni.cicduniversityfriendly.R

enum class UniversityToolbarStyle {
    DEFAULT, LIGHT
}

enum class UniversityToolbarType {
    NO_TOOLBAR, NORMAL, HOME
}

class UniversityToolbar : Toolbar {
    private lateinit var txtMiddleTitle: TextView
    private lateinit var txtLeftTitle: TextView
    private lateinit var btnRight: AppCompatImageView
    private lateinit var btnBack: AppCompatImageView

    constructor(context: Context) : super(context) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context)
    }

    private fun initView(context: Context) {
        inflate(context, R.layout.university_toolbar, this)
        setupUi()
    }

    fun initToolbar(type: UniversityToolbarType, title: String, mode: UniversityToolbarStyle) {
        setToolbarTitle(title)
        when (mode) {
            UniversityToolbarStyle.DEFAULT -> {
                setDefaultToolbarStyle()
                setToolbarType(type)
            }
            UniversityToolbarStyle.LIGHT -> {
                setLightToolbarStyle()
                setToolbarType(type)
            }
        }
    }

    fun initToolbar(type: UniversityToolbarType, mode: UniversityToolbarStyle) {
        when (mode) {
            UniversityToolbarStyle.DEFAULT -> {
                setDefaultToolbarStyle()
                setToolbarType(type)
            }
            UniversityToolbarStyle.LIGHT -> {
                setLightToolbarStyle()
                setToolbarType(type)
            }
        }
    }

    fun hideLeftButton() {
        btnBack.visibility = View.GONE
    }

    fun hideRightButton() {
        btnRight.visibility = View.GONE
    }

    fun showRightButton() {
        btnRight.visibility = View.VISIBLE
    }

    fun showBackButton(onBack: () -> Unit) {
        btnBack.visibility = View.VISIBLE
        btnBack.setOnClickListener {
            onBack()
        }
    }

    fun showRightButton(onClick: () -> Unit) {
        btnRight.visibility = View.VISIBLE
        btnRight.setOnClickListener {
            onClick()
        }
    }

    fun backButtonClicked(onClick: () -> Unit) {
        btnBack.setOnClickListener {
            onClick()
        }
    }

    fun showRightButton(image: Int, onClick: () -> Unit) {
        btnRight.setImageDrawable(ContextCompat.getDrawable(context, image))
        btnRight.visibility = View.VISIBLE
        btnRight.setOnClickListener {
            onClick()
        }
    }

    private fun setupUi() {
        txtMiddleTitle = findViewById(R.id.toolbar_middle_title)
        txtLeftTitle = findViewById(R.id.toolbar_left_title)
        btnBack = findViewById(R.id.toolbar_back_button)
        btnRight = findViewById(R.id.toolbar_right_button)
    }

    fun setToolbarTitle(title: String) {
        txtMiddleTitle.text = title
        txtLeftTitle.text = title
    }

    private fun setLightToolbarStyle() {
        this.background = ColorDrawable(ContextCompat.getColor(context, R.color.white))
        txtLeftTitle.setTextColor(getColor(context, R.color.design_default_color_primary_dark))
        txtMiddleTitle.setTextColor(getColor(context, R.color.design_default_color_primary_dark))
        btnBack.setColorFilter(getColor(context, R.color.design_default_color_primary_dark))
        btnRight.setColorFilter(getColor(context, R.color.design_default_color_primary_dark))
    }

    private fun setDefaultToolbarStyle() {
        this.background = ColorDrawable(getColor(context, R.color.material_on_primary_emphasis_high_type))
        txtLeftTitle.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        txtMiddleTitle.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        btnBack.setColorFilter(ContextCompat.getColor(context, android.R.color.white))
        btnRight.setColorFilter(ContextCompat.getColor(context, R.color.white))
    }

    private fun setToolbarType(type: UniversityToolbarType) {
        when (type) {
            UniversityToolbarType.NORMAL -> {
                setNormalToolbar()
            }
            UniversityToolbarType.HOME -> {
                setHomeToolbar()
            }
            else -> {}
        }
    }

    private fun setHomeToolbar() {
        txtLeftTitle.visibility = View.VISIBLE
        txtMiddleTitle.visibility = View.GONE
        btnBack.visibility = View.GONE
        btnRight.visibility = View.INVISIBLE
    }

    private fun setNormalToolbar() {
        txtLeftTitle.visibility = View.GONE
        txtMiddleTitle.visibility = View.VISIBLE
        btnBack.visibility = View.INVISIBLE
        btnRight.visibility = View.GONE
    }
}