package com.uni.cicduniversityfriendly.authorization

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.junit.Assert.*
import org.junit.Rule
import com.uni.cicduniversityfriendly.R
import org.junit.runner.RunWith
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Test

@RunWith(AndroidJUnit4ClassRunner::class)
class LoginActivityTest {

    @get:Rule var activityLoginScenarioRule = ActivityScenarioRule(LoginActivity::class.java)

    @Test
    fun checkLoginData(){
        onView(withId(R.id.email))
            .perform(typeText("bogdan@gmail.com"), closeSoftKeyboard())

        onView(withId(R.id.password))
            .perform(typeText("Bogdan1!"), closeSoftKeyboard())

        onView(withId(R.id.buttonLogin))
            .perform(click())
    }
}